#!/usr/bin/env sh
if type lsb_release >/dev/null 2>&1 ; then
   distro=$(lsb_release -i -s)
elif [ -e /etc/os-release ] ; then
   distro=$(awk -F= '$1 == "ID" {print $2}' /etc/os-release)
elif [ -e /etc/some-other-release-file ] ; then
   distro=$(ihavenfihowtohandleotherhypotheticalreleasefiles)
fi

# convert to lowercase
distro=$(printf '%s\n' "$distro" | LC_ALL=C tr '[:upper:]' '[:lower:]')
case "$distro" in
   ubuntu*)   apt-get update && apt-get install build-essential python3-dev python3-wheel twine python3-cryptography rustc cargo libssl-dev libffi-dev -y;;
   debian*)   apt-get update && apt-get install build-essential python3-dev python3-wheel twine python3-cryptography rustc cargo libssl-dev libffi-dev -y;;
   alpine*)   apk update && apk add bc cargo gcc libffi-dev musl-dev openssl-dev rust python3-dev py3-pip py3-cryptography py3-twine py3-wheel;;
   *)        echo "unknown distro: '$distro'" ; exit 1 ;;
esac

